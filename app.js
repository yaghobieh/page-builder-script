#! /usr/bin/env node
const fs = require('fs');
const readline = require('readline');
const request = require('request');
const http = require('http');
const https = require('https');
const Stream = require('stream').Transform;

//File name + Create File sys
const fileName = './index.html';
let stream = fs.createWriteStream(fileName); 
let lines = [];

// Build Basic html page
htmlSkeleton = tdCols => {

    return `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
        </head>
        <body>
            <table>
                <tr>
                    <th>Image</th>
                    <th>Original Url</th>
                </tr>
                <tr>
                    ${tdCols}
                </tr>
            </table>
        </body>
        </html>
    `
};

// Create the page
stream.once('open', fd => {

    const rl = readline.createInterface(process.stdin, process.stdout);

    rl.setPrompt('Enter Url (Enter "esc" for exit!): ');
    rl.prompt();

    rl.on('line', line => {

        if (line === "esc") rl.close(); 

        lines.push(line); //Push into Array of images
        downloadImage(line, `public/name.jpg`); //Download Image
        
        rl.prompt();
    }).on('close', () => { 
        let tdCols = createTable();
        let htmlPage = htmlSkeleton(tdCols);

        stream.end(htmlPage);
        process.exit(0);
    });
});

// Download Images from url 
// And save into new path
downloadImage = (url, filename, callback) => {

    var client = http;
    if (url.toString().indexOf("https") === 0){
      client = https;
     }

    client.request(url, function(response) {                                        
      var data = new Stream();                                                    

      response.on('data', function(chunk) {                                       
         data.push(chunk);                                                         
      });                                                                         

      response.on('end', function() {                                             
         fs.writeFileSync(data.read());                               
      });                                                                         
   }).end();
};

// Create table
createTable = () => {
    return lines.map((line) => (
      `
              <td><img src="${line}" style="width: 120px;"></td>
              <td><a href="${line}" target="_blank">Open Original path</a></td>
          `
    )).join("\n");
}